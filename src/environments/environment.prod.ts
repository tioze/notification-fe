// Para fazer o Deploy/Release da aplicação:
// Executar o comando:
// ng build -c prod --base-href
// Copiar o conteúdo gerado na pasta `dist` do projeto.
//
export const environment = {
  production: true,

  // Configurações Aplicação:
  obterTokenUrl: 'authenticate',
  esquecisenha: 'publicador/esquecisenha'
};
