import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { HomeComponent } from './modules/core/home/home.component'
import { TemplateComponent } from './modules/core/template/template.component'

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
   // { path: 'unauthorized', component: UnauthorizedComponent },
    //{ path: 'login', component: LoginComponent },
    {
        path: '',
        //canActivate: [AuthGuardService],
        component: TemplateComponent,
        children: [
            { path: 'home', component: HomeComponent},           
            {
                path: 'notification',
                loadChildren: () => import('./modules/message/message.module').then((m) => m.MessageModule),
            }
        ],
    },
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
