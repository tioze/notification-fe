import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrMaskerModule } from 'br-mask';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { ImageModule } from 'primeng/image';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PaginatorModule } from 'primeng/paginator';
import { PickListModule } from 'primeng/picklist';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [
   
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    CardModule,
    ConfirmDialogModule,
    DialogModule,
    ImageModule,
    InputTextModule,
    InputTextareaModule,
    PaginatorModule,
    ProgressSpinnerModule,
    PickListModule,
    RatingModule,
    RadioButtonModule,
    ScrollPanelModule,
    TableModule,
    ToolbarModule,
    TooltipModule,
    RouterModule,
    RouterModule,
    RadioButtonModule,
    BrMaskerModule,


  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    CardModule,
    ConfirmDialogModule,
    DialogModule,
    ImageModule,
    InputTextModule,
    InputTextareaModule,
    PaginatorModule,
    ProgressSpinnerModule,
    PickListModule,
    RadioButtonModule,
    ScrollPanelModule,
    TableModule,
    TooltipModule,
    ToolbarModule,
    RatingModule,
    RouterModule,
    RouterModule,
    RadioButtonModule,
  ]
})
export class CommonComponentsSharedModule { }
