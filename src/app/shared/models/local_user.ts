export interface LocalUser {
    login: string;
    token: string;   
    uuid: string;  
    nome: string;
    perfil:string; 
}
 