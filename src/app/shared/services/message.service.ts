import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryDto } from '../models/category';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageSentService {

  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/json'),
  };

  constructor(private http: HttpClient) { }

  getCategories(): Observable<CategoryDto[]> {
    return this.http.get<CategoryDto[]>(environment.serverUrl + 'category/all', this.options)
  }

  getHistorics(): Observable<CategoryDto[]> {
    return this.http.get<CategoryDto[]>(environment.serverUrl + 'notificationlog/all', this.options)
  }

  add(message): Observable<any> {
    return this.http.post(environment.serverUrl + 'notification', message, this.options)
  }
}
