import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageRoutingModule } from './message-routing.module';
import { RegisterMessageComponent } from './pages/register-message/register-message.component';
import { HistoricMessageComponent } from './pages/historic-message/historic-message.component';
import { MessageComponent } from './message.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ToolbarModule } from 'primeng/toolbar';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { DropdownModule } from 'primeng/dropdown';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DialogModule } from 'primeng/dialog';

@NgModule({
  declarations: [
    RegisterMessageComponent,
    HistoricMessageComponent,
    MessageComponent
  ],
  imports: [
    CommonModule,
    MessageRoutingModule,
    CommonModule,
    MessageRoutingModule,
    FormsModule,
    CardModule,
    ButtonModule,
    TableModule,
    ToastModule,
    ToolbarModule,
    ConfirmPopupModule,    
    ReactiveFormsModule,
    DropdownModule,   
    TooltipModule,
    InputTextareaModule,
    DialogModule    
  ]
})
export class MessageModule { }
