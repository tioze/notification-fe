import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricMessageComponent } from './historic-message.component';

describe('HistoricMessageComponent', () => {
  let component: HistoricMessageComponent;
  let fixture: ComponentFixture<HistoricMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
