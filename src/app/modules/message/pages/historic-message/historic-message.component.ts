import { Component, OnInit } from '@angular/core';
import { HistoricDto } from 'src/app/shared/models/historic';
import { MessageSentService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-historic-message',
  templateUrl: './historic-message.component.html',
  styleUrls: ['./historic-message.component.css']
})
export class HistoricMessageComponent implements OnInit {

  errors: String[];
  visible: boolean = false;
  category = '';
  date = '';
  message = '';

  historics: HistoricDto[] = [];

    first = 0;

    rows = 10;

    constructor( private messageService: MessageSentService, ) {}

    ngOnInit() {
      this.getHistorics();
      console.log(this.historics)
    }

    getHistorics() {
      this.messageService
      .getHistorics()
      .subscribe(response => {
        this.historics = response;
        console.log(response)
      }, errorResponse => {
          console.log(errorResponse);
          this.errors = ['Fixed an error when searching for historics!']
      });
    }

    next() {
        this.first = this.first + this.rows;
    }

    prev() {
        this.first = this.first - this.rows;
    }

    reset() {
        this.first = 0;
    }

    pageChange(event) {
        this.first = event.first;
        this.rows = event.rows;
    }

    isLastPage(): boolean {
        return this.historics ? this.first === this.historics.length - this.rows : true;
    }

    isFirstPage(): boolean {
        return this.historics ? this.first === 0 : true;
    }


    showDialog(element:any) {
      this.category = element.category;
      this.date = element.datemessagesent;
      this.message = element.message;
        this.visible = true;
    }
}
