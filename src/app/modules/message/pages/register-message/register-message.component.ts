import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CategoryDto } from 'src/app/shared/models/category';
import { MessageSentService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-register-message',
  templateUrl: './register-message.component.html',
  styleUrls: ['./register-message.component.css']
})
export class RegisterMessageComponent implements OnInit {

  cadastroForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  error = '';
  categoryEmpyte = false;
  messageEmpyte = false;

  categories: CategoryDto[] = [];
  errors: String[];

  constructor(private formBuilder: FormBuilder,
    private messageService: MessageSentService,
    private confirmationService: ConfirmationService,
    public message: MessageService) { }

  ngOnInit(): void {
    this.initForm();
    this.getCategories();
  }

  get f() { return this.cadastroForm.controls; }

  initForm() {
    this.cadastroForm = this.formBuilder.group({
      category: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required])
    })
  }

  getCategories() {
    this.messageService
    .getCategories()
    .subscribe(response => {
      this.categories = response;
      console.log(response)
    }, errorResponse => {
        console.log(errorResponse);
        this.errors = ['Fixed an error when searching for categories!']
    });
  }

  register() {

    this.submitted = true;

  if (this.cadastroForm.invalid) {
    this.categoryEmpyte = true;
    this.messageEmpyte = true;
    return; // Prevent submission if form is invalid
  }

  const data = {
    categoryuuid: this.cadastroForm.get('category').value.uuid,
    message: this.cadastroForm.get('message').value
  }
  this.messageService.add(data).subscribe((retorno) => {
    this.message.add({
      severity: 'success',
      summary: 'Sent',
      detail: 'Message sent successfully!',
      life: 3000,
    })
   })
  }
}
