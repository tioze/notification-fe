import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessageComponent } from './message.component';
import { RegisterMessageComponent } from './pages/register-message/register-message.component';
import { HistoricMessageComponent } from './pages/historic-message/historic-message.component';

const routes: Routes = [{
  path: '', component: MessageComponent,
  children: [
    { path: 'register', component: RegisterMessageComponent },      
    { path: 'historic', component: HistoricMessageComponent }   
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageRoutingModule { }
