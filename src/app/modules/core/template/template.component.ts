import { MenuItem } from 'primeng/api'
import { AuthService } from 'src/app/shared/services/auth.service'
import { Router } from '@angular/router'
import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'app-template',
    templateUrl: './template.component.html',
    styleUrls: ['./template.component.css'],
})
export class TemplateComponent implements OnInit {
    nomeUser: string
    _isAuthenticated = false
    menus: MenuItem[];   
    user

    pageSelect = JSON.parse(localStorage.getItem('tela'));  
    telaHome: boolean = false;
    telaCadastrar: boolean = false;
    mensagemCabecalho: string;

    constructor(public router: Router, private oidcAuthService: AuthService) { }    

    ngOnInit(): void {
       // this.user = this.oidcAuthService.getLoggedUser();                
        this.initMenuPrincipal();   
        this.montaIniciaisNome();    
    }    

    get isAutenticated() {
        return this._isAuthenticated
    }

    goToHome() {
        this.router.navigate(['/'])
    }

    logout() {
        this.oidcAuthService.logout();
        this.router.navigate(['/login'])
    }

    montaIniciaisNome() {
        var arrayNome= this.user.nome.split(' ');
        this.nomeUser = arrayNome[0].substr(0, 1).toUpperCase();
        this.nomeUser = this.nomeUser + arrayNome[1].substr(0, 1).toUpperCase();
    }

    initMenuPrincipal() {
       
        //let isAdmin = (this.user.perfil.nome == 'ADMIN' || this.user.perfil.nome == 'ADMINPONTO');
        this.menus = [
            {
                label: 'Home',
                routerLink: ['/home'],
                icon: 'pi pi-home',                
            },           
            { label: 'Notification', icon: 'pi pi-bell', //visible: isAdmin,
                items: [                   
                    {label: 'Register', routerLink: ['/notification/register'], icon: 'pi pi-send'},
                    {label: 'Historic', routerLink: ['/notification/historic'], icon: 'pi pi pi-list'}
                ]
            }  
        ]
    }    
}
