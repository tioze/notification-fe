import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import jwtDecode from "jwt-decode";
import { MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { CredenciaisDTO } from 'src/app/shared/models/credenciais.dto';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [DialogService, MessageService]
})
export class LoginComponent {

  mensagemSucesso: string;
  errors: String[];
  injector: any

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  isForgotPassword = false;
  error = '';

  creds: CredenciaisDTO = {
    username: "",
    password: ""
  };


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    public messageService: MessageService,
    public dialogService: DialogService
  ) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['']
    });
  }

  private get _toastrService(): ToastrService {
    return this.injector.get(ToastrService)
  }

  forgotPassword() {
    this.isForgotPassword = true;
  }

  submitForgotPassword() {
    this.loading = true;
    if (!this.f.email.value) {
      this.loading = false;
      this.errors = ['Fill in your email'];
      return;
    }
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.creds.username = this.f.username.value;
    this.creds.password = this.f.password.value;
  }
}
