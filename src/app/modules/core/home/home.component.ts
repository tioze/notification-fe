import { DatePipe } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { ConfirmationService, MessageService } from 'primeng/api'
import { DividerModule } from 'primeng/divider'
import { DialogService } from 'primeng/dynamicdialog'
import { PanelModule } from 'primeng/panel'
import { LocalUser } from 'src/app/shared/models/local_user'
import { AuthService } from 'src/app/shared/services/auth.service'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [DialogService, MessageService, DividerModule, PanelModule],
})
export class HomeComponent implements OnInit {
   
    public constructor(
             
    ) { }

    ngOnInit() {        
    }

    
}
